#!/bin/bash
sudo yum -y update
sudo yum -y install rsync pykickstart genisoimage
sudo mkdir -p /mnt/iso
sudo mount /dev/sr0 /mnt/iso
sudo mkdir -p /opt/kickstart_build
sudo chown myadmin.myadmin /opt/kickstart_build
cp -r /mnt/iso/* /opt/kickstart_build/
cp /home/myadmin/in_work/kickstart/lab/fusion/isolinux.cfg /opt/kickstart_build/isolinux/
cd /opt/kickstart_build/;mkisofs -o centos-7-custom.iso -b isolinux.bin -c boot.cat -no-emul-boot -V 'CentOS 7 x86_64' -boot-load-size 4 -boot-info-table -R -J -v -T isolinux/. .
