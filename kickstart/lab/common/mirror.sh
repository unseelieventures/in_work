#!/bin/bash
mkdir -p /mnt/vol
mkdir -p /mnt/backup

#Backup
mount /dev/system1/root /mnt/vol
mount /dev/common/backup /mnt/backup
xfsdump -J -F -f /mnt/backup/root.xfs /mnt/vol
umount /mnt/vol

mount /dev/system1/home /mnt/vol
xfsdump -J -F -f /mnt/backup/home.xfs /mnt/vol
umount /mnt/vol

mount /dev/system1/tmp /mnt/vol
xfsdump -J -F -f /mnt/backup/tmp.xfs /mnt/vol
umount /mnt/vol

mount /dev/system1/var /mnt/vol
xfsdump -J -F -f /mnt/backup/var.xfs /mnt/vol
umount /mnt/vol

mount /dev/system1/var_log /mnt/vol
xfsdump -J -F -f /mnt/backup/var_log.xfs /mnt/vol
umount /mnt/vol

mount /dev/system1/var_log_audit /mnt/vol
xfsdump -J -F -f /mnt/backup/var_log_audit.xfs /mnt/vol
umount /mnt/vol

#System 2
mount /dev/system2/root /mnt/vol
xfsrestore -f /mnt/backup/root.xfs /mnt/vol
umount /mnt/vol

mount /dev/system2/home /mnt/vol
xfsrestore -f /mnt/backup/home.xfs /mnt/vol
umount /mnt/vol

mount /dev/system2/tmp /mnt/vol
xfsrestore -f /mnt/backup/tmp.xfs /mnt/vol
umount /mnt/vol

mount /dev/system2/var /mnt/vol
xfsrestore -f /mnt/backup/var.xfs /mnt/vol
umount /mnt/vol

mount /dev/system2/var_log /mnt/vol
xfsrestore -f /mnt/backup/var_log.xfs /mnt/vol
umount /mnt/vol

mount /dev/system2/var_log_audit /mnt/vol
xfsrestore -f /mnt/backup/var_log_audit.xfs /mnt/vol
umount /mnt/vol

#System 3
mount /dev/system3/root /mnt/vol
xfsrestore -f /mnt/backup/root.xfs /mnt/vol
umount /mnt/vol

mount /dev/system3/home /mnt/vol
xfsrestore -f /mnt/backup/home.xfs /mnt/vol
umount /mnt/vol

mount /dev/system3/tmp /mnt/vol
xfsrestore -f /mnt/backup/tmp.xfs /mnt/vol
umount /mnt/vol

mount /dev/system3/var /mnt/vol
xfsrestore -f /mnt/backup/var.xfs /mnt/vol
umount /mnt/vol

mount /dev/system3/var_log /mnt/vol
xfsrestore -f /mnt/backup/var_log.xfs /mnt/vol
umount /mnt/vol

mount /dev/system3/var_log_audit /mnt/vol
xfsrestore -f /mnt/backup/var_log_audit.xfs /mnt/vol
umount /mnt/vol

#System 4
mount /dev/system4/root /mnt/vol
xfsrestore -f /mnt/backup/root.xfs /mnt/vol
umount /mnt/vol

mount /dev/system4/home /mnt/vol
xfsrestore -f /mnt/backup/home.xfs /mnt/vol
umount /mnt/vol

mount /dev/system4/tmp /mnt/vol
xfsrestore -f /mnt/backup/tmp.xfs /mnt/vol
umount /mnt/vol

mount /dev/system4/var /mnt/vol
xfsrestore -f /mnt/backup/var.xfs /mnt/vol
umount /mnt/vol

mount /dev/system4/var_log /mnt/vol
xfsrestore -f /mnt/backup/var_log.xfs /mnt/vol
umount /mnt/vol

mount /dev/system4/var_log_audit /mnt/vol
xfsrestore -f /mnt/backup/var_log_audit.xfs /mnt/vol
umount /mnt/vol

#System 5
mount /dev/system5/root /mnt/vol
xfsrestore -f /mnt/backup/root.xfs /mnt/vol
umount /mnt/vol

mount /dev/system5/home /mnt/vol
xfsrestore -f /mnt/backup/home.xfs /mnt/vol
umount /mnt/vol

mount /dev/system5/tmp /mnt/vol
xfsrestore -f /mnt/backup/tmp.xfs /mnt/vol
umount /mnt/vol

mount /dev/system5/var /mnt/vol
xfsrestore -f /mnt/backup/var.xfs /mnt/vol
umount /mnt/vol

mount /dev/system5/var_log /mnt/vol
xfsrestore -f /mnt/backup/var_log.xfs /mnt/vol
umount /mnt/vol

mount /dev/system5/var_log_audit /mnt/vol
xfsrestore -f /mnt/backup/var_log_audit.xfs /mnt/vol
umount /mnt/vol
umount /mnt/backup
