#!/bin/bash
sudo yum remove docker docker-common docker-selinux docker-engine
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
#sudo yum install docker-ce
sudo yum install docker-1.12.6
sudo useradd -G docker myadmin
sudo systemctl start docker
sudo docker pull golang:latest
