#!/bin/bash
yum install -y iptables-services
# If connecting remotely we must first temporarily set the default policy on the INPUT chain to ACCEPT otherwise once we flush the current rules we will be locked out of our server.
iptables -P INPUT ACCEPT

# We used the -F switch to flush all existing rules so we start with a clean state from which to add new rules.
iptables -F

# We use the -A switch to append (or add) a rule to a specific chain

# Always accept things going over the loopback interface
# This is to allow a jump point for know source addresses to note get logged
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Begin Inbound Application rules
iptables -A OUTPUT -p tcp --sport 22 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT
# End Inbound Application rules

# Begin Outbound Application rules
# Allow dns client requests
iptables -A OUTPUT -p udp --sport 1024:65535 --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p udp --sport 53 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 1024:65535 --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --sport 53 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
# Allow ntp client requests
iptables -A OUTPUT -p udp --sport 1024:65535 --dport 123 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p udp --sport 123 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
# Allow dhcp requests
iptables -A INPUT -p udp --sport 67 --dport 68 -j ACCEPT
iptables -A OUTPUT -p udp --sport 68 --dport 67 -j ACCEPT
# Allow http for apt-get
iptables -A OUTPUT -p tcp --sport 1024:65535 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --sport 80 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 1024:65535 --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --sport 443 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
# End Outbound Application rules
# Restrict ICMP Message Types
iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
# Log and Drop Packets with Suspicious Source Addresses
iptables -A INPUT -i eth0 -s 10.0.0.0/8 -j LOG --log-prefix "IP DROP SPOOF A: "
#iptables -A INPUT -i eth0 -s 172.16.0.0/12 -j LOG --log-prefix "IP DROP SPOOF B: "
iptables -A INPUT -i eth0 -s 192.168.0.0/16 -j LOG --log-prefix "IP DROP SPOOF C: "
iptables -A INPUT -i eth0 -s 224.0.0.0/4 -j LOG --log-prefix "IP DROP MULTICAST D: "
iptables -A INPUT -i eth0 -s 240.0.0.0/5 -j LOG --log-prefix "IP DROP SPOOF E: "
iptables -A INPUT -i eth0 -d 127.0.0.0/8 -j LOG --log-prefix "IP DROP LOOPBACK: "
#Begin Inbound Application rules
iptables -A OUTPUT -p tcp --sport 80 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 80 --sport 1024:65535 -m state --state NEW,ESTABLISHED -j ACCEPT

iptables -N BADFLAGS
iptables -A BADFLAGS -j LOG --log-prefix "IPT BADFLAGS: "
iptables -A BADFLAGS -j DROP

# Make sure NEW incoming tcp connections are SYN packets; otherwise we need to drop them
# This provides SYN flood protection
iptables -A INPUT -p tcp ! --syn -m state --state NEW -j BADFLAGS
# Packets with incoming fragments drop them. This attack result into Linux server panic such data loss.
iptables -A INPUT -f -j BADFLAGS

iptables -N TCP_FLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags ACK,FIN FIN -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags ACK,PSH PSH -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags ACK,URG URG -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags FIN,RST FIN,RST -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags SYN,FIN SYN,FIN -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags SYN,RST SYN,RST -j BADFLAGS
# drop Incoming malformed XMAS packets
iptables -A TCP_FLAGS -p tcp --tcp-flags ALL ALL -j BADFLAGS
# drop Incoming malformed NULL packets
iptables -A TCP_FLAGS -p tcp --tcp-flags ALL NONE -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags ALL FIN,PSH,URG -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j BADFLAGS
iptables -A TCP_FLAGS -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j BADFLAGS

# This rule allows all established related and is too broad
#iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

#Block logging of this traffic to prevent filling logs

#Get broadcast address
IP=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $4}')
iptables -A INPUT -m pkttype --pkt-type broadcast -d $IP/32 -j DROP
iptables -A INPUT -m pkttype --pkt-type broadcast -d 255.255.255.255/32 -j DROP
iptables -A INPUT -p igmp -d 244.0.0.1 -j DROP

# Log and Drop all remaining traffic
iptables -A INPUT -j LOG
iptables -A INPUT -j DROP
iptables -A OUTPUT -j LOG
iptables -A OUTPUT -j DROP

iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

iptables-save > /etc/sysconfig/iptables
systemctl enable iptables.service
