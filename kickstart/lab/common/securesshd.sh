#!/bin/bash

function replace_or_append {
  local config_file=$1
  local key=$2
  local value=$3
  local cce=$4
  local format=$5

  # Check sanity of the input
  if [ $# -lt "3" ]
  then
        echo "Usage: replace_or_append 'config_file_location' 'key_to_search' 'new_value'"
        echo
        echo "If symlinks need to be taken into account, add yes/no to the last argument"
        echo "to allow to 'follow_symlinks'."
        echo "Aborting."
        exit 1
  fi
  
  # Test that the cce arg is not empty or does not equal @CCENUM@.
  # If @CCENUM@ exists, it means that there is no CCE assigned.
  if ! [ "x$cce" = x ] && [ "$cce" != '@CCENUM@' ]; then
    cce="CCE-${cce}"
  else
    cce="CCE"
  fi
  
  # Strip any search characters in the key arg so that the key can be replaced without
  # adding any search characters to the config file.
  stripped_key=$(sed "s/[\^=\$,;+]*//g" <<< $key)
  commented_key="#$stripped_key"

  # If there is no print format specified in the last arg, use the default format.
  if ! [ "x$format" = x ] ; then
    printf -v formatted_output "$format" "$stripped_key" "$value"
  else
    formatted_output="$stripped_key = $value"
  fi
  
  local symbolic=false
  # Test if the config_file is a symbolic link. If so, use --follow-symlinks with sed.
  # Otherwise, regular sed command will do.
  if test -L $config_file; then
     symbolic=true
  fi
    
  if `grep -qi $key $config_file` ; then
     # Test if the config_file is a symbolic link. If so, use --follow-symlinks with sed.
     # Otherwise, regular sed command will do.
     if [ "$symbolic" = true ] ; then
       sed -i --follow-symlinks "/$key.*/d" $config_file
       sed -i --follow-symlinks "/$commented_key.*/d" $config_file
       sed -i --follow-symlinks "/$formatted_output.*/d" $config_file
     else
       sed -i "/$key.*/d" $config_file
       sed -i "/$commented_key.*/d" $config_file
       sed -i "/$formatted_output.*/d" $config_file
     fi
   fi
   
   #Add back the correct directive
   # \n is precaution for case where file ends without trailing newline
   echo -e "\n# Per $cce: Set $formatted_output in $config_file" >> $config_file
   echo -e "$formatted_output" >> $config_file

   #Remove double newlines
   if [ "$symbolic" = true ] ; then
     sed --follow-symlinks -ie $config_file -e :n -e 'N;s/\n$//;tn'
   else
     sed -ie $config_file -e :n -e 'N;s/\n$//;tn'
   fi
}

function disable {
  local config_file=$1
  local key=$2
  local value=$3
  local cce=$4
  local format=$5

  # Check sanity of the input
  if [ $# -lt "3" ]
  then
        echo "Usage: replace_or_append 'config_file_location' 'key_to_search' 'new_value'"
        echo
        echo "If symlinks need to be taken into account, add yes/no to the last argument"
        echo "to allow to 'follow_symlinks'."
        echo "Aborting."
        exit 1
  fi
  
  # Test that the cce arg is not empty or does not equal @CCENUM@.
  # If @CCENUM@ exists, it means that there is no CCE assigned.
  if ! [ "x$cce" = x ] && [ "$cce" != '@CCENUM@' ]; then
    cce="CCE-${cce}"
  else
    cce="CCE"
  fi
  
  # Strip any search characters in the key arg so that the key can be replaced without
  # adding any search characters to the config file.
  stripped_key=$(sed "s/[\^=\$,;+]*//g" <<< $key)
  commented_key="#$stripped_key"

  # If there is no print format specified in the last arg, use the default format.
  if ! [ "x$format" = x ] ; then
    printf -v formatted_output "$format" "$stripped_key" "$value"
  else
    formatted_output="$stripped_key = $value"
  fi
  
  local symbolic=false
  # Test if the config_file is a symbolic link. If so, use --follow-symlinks with sed.
  # Otherwise, regular sed command will do.
  if test -L $config_file; then
     symbolic=true
  fi
    
  if `grep -qi $key $config_file` ; then
     # Test if the config_file is a symbolic link. If so, use --follow-symlinks with sed.
     # Otherwise, regular sed command will do.
     if [ "$symbolic" = true ] ; then
       sed -i --follow-symlinks "/$key.*/d" $config_file
       sed -i --follow-symlinks "/$commented_key.*/d" $config_file
       sed -i --follow-symlinks "/$formatted_output.*/d" $config_file
     else
       sed -i "/$key.*/d" $config_file
       sed -i "/$commented_key.*/d" $config_file
       sed -i "/$formatted_output.*/d" $config_file
     fi
   fi
   
   #Add back the correct directive
   # \n is precaution for case where file ends without trailing newline
   echo -e "\n# Per $cce: Set #$formatted_output in $config_file" >> $config_file
   echo -e "#$formatted_output" >> $config_file

   #Remove double newlines
   if [ "$symbolic" = true ] ; then
     sed --follow-symlinks -ie $config_file -e :n -e 'N;s/\n$//;tn'
   else
     sed -ie $config_file -e :n -e 'N;s/\n$//;tn'
   fi
}

config_file='/etc/ssh/sshd_config'

disable $config_file '^RhostsRSAAuthentication' 'Deprecated' 'CCE-80373-4' '%s %s'
#replace_or_append $config_file '^Ciphers' 'aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc,aes192-cbc,aes256-cbc' 'CCE-27295-5' '%s %s'
replace_or_append $config_file '^Ciphers' 'chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr' 'CCE-27295-5 plus' '%s %s'
replace_or_append $config_file '^PermitUserEnvironment' 'no' 'CCE-27363-1' '%s %s'
replace_or_append $config_file '^PermitEmptyPasswords' 'no' 'CCE-27471-2' '%s %s'
replace_or_append $config_file '^IgnoreRhosts' 'yes' 'CCE-27377-1' '%s %s'
replace_or_append $config_file '^ClientAliveCountMax' '0' 'CCE-27082-7' '%s %s'
replace_or_append $config_file '^ClientAliveInterval' '600' 'CCE-27433-2' '%s %s'
replace_or_append $config_file '^Protocol' '2' 'CCE-27320-1' '%s %s'
replace_or_append $config_file '^GSSAPIAuthentication' 'no' 'CCE-80220-7' '%s %s'
replace_or_append $config_file '^KerberosAuthentication' 'no' 'CCE-80221-5' '%s %s'
replace_or_append $config_file '^StrictModes' 'yes' 'CCE-80222-3' '%s %s'
replace_or_append $config_file '^UsePrivilegeSeparation' 'sandbox' 'CCE-80223-1' '%s %s'
replace_or_append $config_file '^Compression' 'no' 'CCE-80224-9' '%s %s'
replace_or_append $config_file '^IgnoreUserKnownHosts' 'yes' 'CCE-80372-6' '%s %s'
replace_or_append $config_file '^HostBasedAuthentication' 'no' 'CCE-27413-4' '%s %s'
replace_or_append $config_file '^X11Forwarding' 'yes' 'CCE-80226-4' '%s %s'
replace_or_append $config_file '^PermitRootLogin' 'no' 'CCE-27445-6' '%s %s'
replace_or_append $config_file '^PermitEmptyPasswords' 'no' 'CCE-27471-2' '%s %s'
replace_or_append $config_file '^MACs' 'hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com' 'CCE-27455-5 plus' '%s %s'
replace_or_append $config_file '^KexAlgorithms' 'curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256' 'CCE-' '%s %s'
replace_or_append $config_file '^PasswordAuthentication' 'no' 'CCE-' '%s %s'
replace_or_append $config_file '^ChallengeResponseAuthentication' 'no' 'CCE-' '%s %s'
replace_or_append $config_file '^PubkeyAuthentication' 'yes' 'CCE-' '%s %s'
replace_or_append $config_file '^AllowGroups' 'ssh-user' 'CCE-' '%s %s'

sudo groupadd ssh-user
sudo usermod -a -G ssh-user myadmin

# Set Banner
sed -i '/^Banner.*/d' $config_file
sed -i '/^#Banner.*/d' $config_file
echo -e 'Banner /etc/issue' >> $config_file

# Set HostKey entries
sed -i '/^HostKey.*/d' $config_file
sed -i '/^#HostKey.*/d' $config_file
echo -e "HostKey /etc/ssh/ssh_host_ed25519_key" >> $config_file
echo -e "HostKey /etc/ssh/ssh_host_rsa_key" >> $config_file

rm /etc/ssh/ssh_host_*key*
ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N "" < /dev/null
ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N "" < /dev/null

# CCE-27311-0
sudo chmod 0644 /etc/ssh/*.pub
# CCE-27485-2
sudo chmod 0600 /etc/ssh/*_key
