#!/bin/bash
# Install Openshift Prequisites
sudo yum install wget git net-tools bind-utils iptables-services bridge-utils bash-completion kexec-tools sos psacct
sudo yum update
# Remove Docker if present
sudo yum remove docker docker-common docker-selinux docker-engine
# Install The required Docker version for Openshift
sudo yum install docker-1.12.6
sudo rpm -V docker-1.12.6

# cat <<EOF > /etc/sysconfig/docker-storage-setup
DEVS=/dev/vdc
VG=docker-vg
EOF

sudo docker-storage-setup
sudo systemctl enable docker
sudo systemctl start docker
sudo systemctl is-active docker
