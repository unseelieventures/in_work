#!/bin/bash
sudo yum -y update
sudo systemctl stop firewalld
sudo systemctl disable firewalld

#System 2
sudo lvcreate --size 2048 --name root system2
sudo lvcreate --size 50 --name home system2
sudo lvcreate --size 100 --name tmp system2
sudo lvcreate --size 500 --name var system2
sudo lvcreate --size 100 --name var_log system2
sudo lvcreate --size 100 --name var_log_audit system2

#System 3
sudo lvcreate --size 2048 --name root system3
sudo lvcreate --size 50 --name home system3
sudo lvcreate --size 100 --name tmp system3
sudo lvcreate --size 500 --name var system3
sudo lvcreate --size 100 --name var_log system3
sudo lvcreate --size 100 --name var_log_audit system3

#System 4
sudo lvcreate --size 2048 --name root system4
sudo lvcreate --size 50 --name home system4
sudo lvcreate --size 100 --name tmp system4
sudo lvcreate --size 500 --name var system4
sudo lvcreate --size 100 --name var_log system4
sudo lvcreate --size 100 --name var_log_audit system4

#System 5
sudo lvcreate --size 2048 --name root system5
sudo lvcreate --size 50 --name home system5
sudo lvcreate --size 100 --name tmp system5
sudo lvcreate --size 500 --name var system5
sudo lvcreate --size 100 --name var_log system5
sudo lvcreate --size 100 --name var_log_audit system5

#Backup
sudo lvcreate --size 2048 --name backup common
sudo lvextend -l 100%FREE /dev/common/backup

sudo mkfs.xfs -L root /dev/system2/root
sudo mkfs.xfs -L home /dev/system2/home
sudo mkfs.xfs -L tmp /dev/system2/tmp
sudo mkfs.xfs -L var /dev/system2/var
sudo mkfs.xfs -L vlog /dev/system2/var_log
sudo mkfs.xfs -L vlog_audit /dev/system2/var_log_audit

sudo mkfs.xfs -L root /dev/system3/root
sudo mkfs.xfs -L home /dev/system3/home
sudo mkfs.xfs -L tmp /dev/system3/tmp
sudo mkfs.xfs -L var /dev/system3/var
sudo mkfs.xfs -L vlog /dev/system3/var_log
sudo mkfs.xfs -L vlog_audit /dev/system3/var_log_audit

sudo mkfs.xfs -L root /dev/system4/root
sudo mkfs.xfs -L home /dev/system4/home
sudo mkfs.xfs -L tmp /dev/system4/tmp
sudo mkfs.xfs -L var /dev/system4/var
sudo mkfs.xfs -L vlog /dev/system4/var_log
sudo mkfs.xfs -L vlog_audit /dev/system4/var_log_audit

sudo mkfs.xfs -L root /dev/system5/root
sudo mkfs.xfs -L home /dev/system5/home
sudo mkfs.xfs -L tmp /dev/system5/tmp
sudo mkfs.xfs -L var /dev/system5/var
sudo mkfs.xfs -L vlog /dev/system5/var_log
sudo mkfs.xfs -L vlog_audit /dev/system5/var_log_audit

sudo mkfs.xfs -L backup /dev/common/backup
