# SCAP Security Guide OSPP/USGCB profile kickstart for Red Hat Enterprise Linux 7 Server
# Version: 0.0.2
# Date: 2015-11-19
#
# Based on:
# http://fedoraproject.org/wiki/Anaconda/Kickstart
# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Installation_Guide/sect-kickstart-syntax.html
# http://usgcb.nist.gov/usgcb/content/configuration/workstation-ks.cfg

# Install a fresh new system (optional)
install

# Use text install
text

# Run the Setup Agent on first boot
firstboot --disable

# Specify installation method to use for installation
# To use a different one comment out the 'url' one below, update
# the selected choice with proper options & un-comment it
#
# Install from an installation tree on a remote server via FTP or HTTP:
# --url		the URL to install from
#
# Example:
#
# url --url=http://192.168.122.1/image
#
# Modify concrete URL in the above example appropriately to reflect the actual
# environment machine is to be installed in
#
# Other possible / supported installation methods:
# * install from the first CD-ROM/DVD drive on the system:
#
cdrom
#
# * install from a directory of ISO images on a local drive:
#
# harddrive --partition=hdb2 --dir=/tmp/install-tree
#
# * install from provided NFS server:
#
# nfs --server=<hostname> --dir=<directory> [--opts=<nfs options>]
#

# Set language to use during installation and the default language to use on the installed system (required)
lang en_US.UTF-8

# Set system keyboard type / layout (required)
keyboard us

# Configure network information for target system and activate network devices in the installer environment (optional)
# --onboot	enable device at a boot time
# --device	device to be activated and / or configured with the network command
# --bootproto	method to obtain networking configuration for device (default dhcp)
# --noipv6	disable IPv6 on this device
#
# NOTE: Usage of DHCP will fail CCE-27021-5 (DISA FSO RHEL-06-000292). To use static IP configuration,
#       "--bootproto=static" must be used. For example:
# network --bootproto=static --ip=10.0.2.15 --netmask=255.255.255.0 --gateway=10.0.2.254 --nameserver 192.168.2.1,192.168.3.1
#
# network --onboot yes --device eth0 --bootproto dhcp --noipv6
network  --bootproto=dhcp --device=ens33 --noipv6 --activate
network  --hostname=localhost.localdomain


# Set the system's root password (required)
# Plaintext password is: server
# Refer to e.g. http://fedoraproject.org/wiki/Anaconda/Kickstart#rootpw to see how to create
# encrypted password form for different plaintext password
rootpw --iscrypted $6$vLT3VoJZv4J37lBq$5koOXI.9UJY97psmH5rebdQ2stWc.s2WakrVi1AL5uPMsVOB/rPC8L/7mF65Or0r.9fTKGzDw.ZGkuqmnk9YH0

# The selected profile will restrict root login
# Add a user that can login and escalate privileges

user --groups=wheel --name=myadmin --password=$6$TMBzihyO3YePF3UV$glS3cKcPCsne1XDUmBRmu1Ssug0L2zgxKohsryXuepgyHgJBSj5MOXoDQh5K.Zn11OWPPFWiAdJS5v/xXzy/e/ --iscrypted --gecos="My Admin"

# Configure firewall settings for the system (optional)
# --enabled	reject incoming connections that are not in response to outbound requests
# --ssh		allow sshd service through the firewall
firewall --enabled --ssh

# Set up the authentication options for the system (required)
# --enableshadow	enable shadowed passwords by default
# --passalgo		hash / crypt algorithm for new passwords
# See the manual page for authconfig for a complete list of possible options.
authconfig --enableshadow --passalgo=sha512

# State of SELinux on the installed system (optional)
# Defaults to enforcing
selinux --enforcing

# Set the system time zone (required)
timezone --utc America/New_York

# Specify how the bootloader should be installed (required)
# Plaintext password is: password
# Refer to e.g. http://fedoraproject.org/wiki/Anaconda/Kickstart#rootpw to see how to create
# encrypted password form for different plaintext password
# bootloader --location=mbr --append="crashkernel=auto rhgb quiet" --password=$6$rhel6usgcb$kOzIfC4zLbuo3ECp1er99NRYikN419wxYMmons8Vm/37Qtg0T8aB9dKxHwqapz8wWAFuVkuI/UJqQBU92bA5C0

bootloader --append=" crashkernel=auto rhgb quiet" --location=mbr --boot-drive=sda

#ignoredisk --only-use=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
ignoredisk --only-use=sda

# Initialize (format) all disks (optional)
zerombr

# The following partition layout scheme assumes disk of size 20GB or larger
# Modify size of partitions appropriately to reflect actual machine's hardware
# 
# Remove Linux partitions from the system prior to creating new ones (optional)
# --linux	erase all Linux partitions
# --initlabel	initialize the disk label to the default based on the underlying architecture
# clearpart --linux --initlabel

#clearpart --all --initlabel --drives=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
clearpart --all --initlabel --drives=sda

# Create primary system partitions (required for installs)
# part /boot --fstype=xfs --size=512
# part pv.01 --grow --size=1

#part /boot --fstype="xfs" --size=500 --ondisk=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
#part /boot/efi --fstype="efi" --size=200 --fsoptions="umask=0077,shortname=winnt" --ondisk=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
#part pv.00 --fstype="lvmpv" --size=1536 --ondisk=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
#part pv.01 --fstype="lvmpv" --size=4096 --ondisk=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
#part pv.02 --fstype="lvmpv" --size=4096 --ondisk=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0
#part pv.03 --fstype="lvmpv" --size=4096 --ondisk=disk/by-id/usb-SanDisk_Ultra_Fit_4C531001450905106365-0:0


part /boot --fstype="xfs" --size=500 --ondisk=sda
part /boot/efi --fstype="efi" --size=200 --fsoptions="umask=0077,shortname=winnt" --ondisk=sda
part pv.00 --fstype="lvmpv" --size=1536 --ondisk=sda
part pv.01 --fstype="lvmpv" --size=4096 --ondisk=sda
part pv.02 --fstype="lvmpv" --size=4096 --ondisk=sda
part pv.03 --fstype="lvmpv" --size=4096 --ondisk=sda

# Create a Logical Volume Management (LVM) group (optional)
# volgroup VolGroup --pesize=4096 pv.01

volgroup common --pesize=4096 pv.00
volgroup system1 --pesize=4096 pv.01
volgroup system2 --pesize=4096 pv.02
volgroup system3 --pesize=4096 pv.03

#logvol /  --fstype="xfs" --size=8192 --name=root --vgname=system1 --grow
# CCE-26557-9: Ensure /home Located On Separate Partition
#logvol /home  --fstype="xfs" --size=1024 --name=home --vgname=system1 --fsoptions="nodev"
# CCE-26435-8: Ensure /tmp Located On Separate Partition
#logvol /tmp  --fstype="xfs" --size=1024 --name=tmp --vgname=system1 --fsoptions="nodev,noexec,nosuid"
# CCE-26639-5: Ensure /var Located On Separate Partition
#logvol /var  --fstype="xfs" --size=2048 --name=var --vgname=system1 --fsoptions="nodev"
# CCE-26215-4: Ensure /var/log Located On Separate Partition
#logvol /var/log  --fstype="xfs" --size=1024 --name=var_log --vgname=system1 --fsoptions="nodev"
# CCE-26436-6: Ensure /var/log/audit Located On Separate Partition
#logvol /var/log/audit  --fstype="xfs" --size=512 --name=var_log_audit --vgname=system1 --fsoptions="nodev"
# Seperate Partition for docker
#logvol /var/lib/docker  --fstype="xfs" --size=2048 --name=var_lib_docker --vgname=system1 --fsoptions="nodev"
# Seperate Partition for optional software
#logvol /opt  --fstype="xfs" --size=4096 --name=opt --vgname=system1 --fsoptions="nodev"
#logvol swap  --fstype="swap" --size=8000 --name=swap --vgname=common


logvol /  --fstype="xfs" --size=2048 --name=root --vgname=system1 --grow
# CCE-26557-9: Ensure /home Located On Separate Partition
logvol /home  --fstype="xfs" --size=50 --name=home --vgname=system1 --fsoptions="nodev"
# CCE-26435-8: Ensure /tmp Located On Separate Partition
logvol /tmp  --fstype="xfs" --size=100 --name=tmp --vgname=system1 --fsoptions="nodev,noexec,nosuid"
# CCE-26639-5: Ensure /var Located On Separate Partition
logvol /var  --fstype="xfs" --size=250 --name=var --vgname=system1 --fsoptions="nodev"
# CCE-26215-4: Ensure /var/log Located On Separate Partition
logvol /var/log  --fstype="xfs" --size=100 --name=var_log --vgname=system1 --fsoptions="nodev"
# CCE-26436-6: Ensure /var/log/audit Located On Separate Partition
logvol /var/log/audit  --fstype="xfs" --size=100 --name=var_log_audit --vgname=system1 --fsoptions="nodev"
# Seperate Partition for docker
logvol /var/lib/docker  --fstype="xfs" --size=50 --name=var_lib_docker --vgname=common --fsoptions="nodev"
# Seperate Partition for optional software
logvol /opt  --fstype="xfs" --size=50 --name=opt --vgname=common --fsoptions="nodev"
logvol swap  --fstype="swap" --size=1024 --name=swap --vgname=common

%addon org_fedora_oscap
        content-type = scap-security-guide
        profile = ospp-rhel7
%end

# Packages selection (%packages section is required)
%packages

# Require @Base
@^minimal
@core
chrony
kexec-tools

# Install selected additional packages (required by profile)
# CCE-27024-9: Install AIDE
aide

# Install libreswan package
libreswan

%end # End of %packages section

%post
exec < /dev/tty3 > /dev/tty3
chvt 3
echo
echo "################################"
echo "# Running Post Configuration   #"
echo "################################"
(
echo 'Install git and Begin Execution of Node 0 post.sh script'
yum -y update
yum -y install git

cat << END_GIT_KEY >> /tmp/git.key
-----BEGIN RSA PRIVATE KEY-----
MIIJKAIBAAKCAgEAyir09hzm4uv+QilzEk03xLgA6Noip0mC7wDxRe5253QXSXpF
+F/uNPJFOdVpNEdcMERhWm2h/ONw7mj/3NrOL8yQi1crHa0OUkH8x6DZG6D4amA1
Jpv9Kh0Ls1i2Op+UJGddmRHqiG0uqILWy4x9Wobfp4t62kkMAOHOPYDpXUWHHBQ/
ruDMWy2WXq/0WW9/nTBgp35q6YqS1g0hnRxA9xQMzY8zQ+VRlOyIT89TJDM93CMU
gvi5kMMh/egvWyDCKQ+ojcOH4ouvhpZ90gsfjFow3a59Lx1PmmL8FuTsDzAEVDKN
z8ScjfhbRO+2WVoemcGXgxIR9rVp2A7yF11a9qC5q3/A7384xuzTPq7Pl/wTVEQq
I2ueZkZhX7vDWZlzAPlnLL3bLF8CfKlBcPXTRn0DDJ7eKjJ+G/Dxb49rJrU6r9s4
9IzC6AX+91TuLQy+TCzOXJVmoW+RNZkabrcW5TpYrRzVYaf6LQGLJ5Bp74rsL3ON
0rQ9sSwiIOtlWR7/M8NqN+9s2yu0la3zGe0r4yXRcORNizRTZZrttDgclaS5HNzN
ZeXPQNKCzMlVhktEZXWfwe9f8BCFt0mmXsCRUpJhK/3UtrvN35zCFscxylqmQTS1
YK5e6Q2HpOLqg7OojYYDuQj43Mc1hDbqME24wKgyxZsve6U42n5foi1NsTkCAwEA
AQKCAgEAlOGTqldYdQ6EHSc3NeglxLoY+WIDGR0aznXsOKrHZ+XnbFYehy3mV5l+
/3s5UaWnr2f8JcMNAxZfyUMMAhIku7CzVdoox171aABubnj2ydD+gCA/pCJGCyh+
wGH5OSzhxPlWRW6bQELflHteQWJBwlQHCvS90Vjv/QpNfp+bu/OC71u+FkQ4WoE0
9qAvm1lwKktBMrf3det1/SqIcZIoCYz7bLojb8HGFZ0nOVceDE+sI5y180nGJmR9
eOXvUfXK5sBASb4QKPbjmYLkP7WvUwXsbKSeTf8Q60owCZAmg3zCA1uDcdmBzU9z
r021BRJH7MVTldPN5NhujQ1cnXZDkINzsHdefB5yda2Z/RxV38zE0BNqQkbtNMx4
7VN0ZDtyZZhaGcXAjUKARNgaDFm1sbMXtur9lWmXgmH61sYlIuSq+Sgmmnr28kwb
jr1cXcXtbG0xltIfBsHTdAN5A2pTvgoLGGybAhMG/2kdSM47Af7HGPZyVmbr+rHa
n/aVPA2oLaRh3qVodFHXPTxUcsNGKvWZsJ5Q59qnE3TzpP/1Av9aJXdokxwgNgK7
5a+0O39wvRRDOOLIkzkpR0Tl9PBJZWuMLlRkW14IAwOt2fRRKunB/AA/Ln4s5wnu
gb24WJO07WCHrMrjsRgX3wgmicza2Kt3/hPZnz4PXVHTb9rjtYECggEBAOO0zp2R
1YMzGNFbDF15DQHXQZqjVH16xn3yAKyI8Qv822CtJxjSqY3BbMynF8sJzfPVKBoO
OvpBMEi5UAfEQebmr9J/cMx1w7DAL9z9HQ/goFf02scR7HhpMoQptRqQQ6XqIh3V
CVMXr62elwOLWXD3WahB10k5u3BdKKPTPj2rF7ymB1CC7CFSuYMyv6wOuyuXmbRv
swbzqb9Sm5t8EvEAo9SU0I4vmDPSSrVyWR2DJXoR6h0EmX43OBIs1Rr5Sw63wMIE
R5tbrj122izSK0yha7TK402TdzUIpuiGB9ls2REM0s1IACJNOZVssOqbZJclE2CB
PGEXrZSXzksAM5ECggEBAONJyXV/751DfXeBdDtv0oNEaXs2lpJFnYkf7iqrLG32
H23Wgrha4Cq+t+IVMre/bCFbSVrDO+BFuctb3Vk9czHhHwVeSzRjacVaDJrVI9si
j6r02F4u+m+vIkVz+tS38A7AvEY8Q4ct72f7LEv9FwnqcK0KJs+WdIyBtRV/UxL/
G2GzN1Lp50NPXnKNOWVXqTj3ekm3gRgVPtCX+ODklJt5WP3K6NagnjaT578rBDXV
ABJMFHXIwoqUuSNtwKEvKl0YuBLVxtodXz1qjhosiN9pKmCb9MPFMfTlxPYXHTf9
I/4fOzcW6SyLBp/pbd11pXyVXa4TnzIPXfM+o8Ro/ykCggEAE1ls6UTYMOLpq/ih
r1u2+D4TbBGO/26saU1kM0UbvpoJBkkQdnKIR+x001srkWxO3PZzBuvIpTVe5f8h
RtethfJ6ZFfB/Q3QyDc8mfnEO3IqDe9Xm+zw/DZsPYDRy+UQaovfxEszFKWBny5H
xNxViFzseHjH04gdXmn8JxevRUxBox00p+nIKPi/nN92HCfQTd7/B68qNmAEP6bE
ZE0Bywrz4P+YTUzMETNAd/S0j+ZyC9Jz421dobUNhbXbkgQ3FZQVxCba8t+yibSM
y5w8Y7Gf1mgbMp9iVNZcYIvrJNW0CFffneQNZtzLFOB8lCMKRDG5WM0ClXmOnX7c
39CvkQKCAQBaso8ZvDu53byre5UOQskWaK5O5LGhHPaISZ2YmUXZe2V/RyJSvrDF
XeWy6hKbEF/tz6et+EpkbG5/n+DsQ4E8XkEquz33YdRLVGvDPVpZj6kU5y+QJd75
wkFCxc1KJTw+Jtr6jAHGZ6YpMCm1CQGwi0T7SBnErJ1FlFzWf0B/TB15FzL0ezI1
we5Gd+R3dEuq19aMpBV3rsKqKnq+A3jpsbq5S8cuHloEHXFU7612bdDhfEqN1dcL
xd07Lgi4SUH2dG6/1V7MmvvuXh7mgu77d9PZSWKa3I3a0/WM3hxiwzkhpwTSLupN
RA0YsuWMge3PkSsY0+uq5kbvUge30M5BAoIBAHg1JKvvSwmUylrvrntm8bDZ8WPr
YYnn6e9XnuDUQWnkuUjORHooyrBlG1FMyDjiX9JJMMAMeD4JAcpgNueiGRNlJXS7
oLPCIByzA5AWh7C3T1IBNn38YtZ1bH+7Ju6tM55bKi1bBXkmorvkEwHUHGOSOSGT
6wC8K/K3fJGeeunUalRYML2O7LDVpVX1VPNFZdLFuoXbBQ4xmYRHJbNkl0s/E/QR
F5iLF658JQ/qkkhyibckS9KiNCOd62/wLwePSE6F2Nk8y4oud2Iwt9pYdTqYxKBy
7/wP2izvffKsnQJI9jY+rI7lguchX3z1yOyshUvc2zcjJKdRCj1lgfWA0vg=
-----END RSA PRIVATE KEY-----
END_GIT_KEY

cd /tmp/;ssh-agent bash -c 'ssh-add /tmp/git.key; git clone git@bitbucket.org:unseelieventures/in_work.git'
rm -f /tmp/git.key
/tmp/in_work/kickstart/lab/node0/post.sh
rm -rf /tmp/in_work
) 2>&1 | /usr/bin/tee /var/log/post_install.log
chvt 1

%end # End of %post section

%anaconda
pwpolicy root --minlen=15 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=15 --minquality=1 --notstrict --nochanges --notempty
pwpolicy luks --minlen=15 --minquality=1 --notstrict --nochanges --notempty
%end # End of %anaconda section

# Reboot after the installation is complete (optional)
# --eject	attempt to eject CD or DVD media before rebooting
reboot --eject
