#!/bin/bash
docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:latest go build -v
docker build --tag myapp .
docker stop myapp;  docker rm -v myapp; docker run -p 8080:8080 -d --name myapp myapp
