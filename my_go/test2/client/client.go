package main

import (
    "net/http"
    "time"
    "fmt"
)

type Client struct {
        // Place holder
        s string       
}

func newClient() *Client {
        return &Client{
        }
}

// start clients to make requests using remaing cores
func (c *Client) run() {
    time.Sleep(100 * time.Millisecond) // Wait for Server to start
    for {
    	resp, err := http.Get("http://172.17.0.2:8080")	
    	if err != nil {
    			fmt.Println("Client: Failed to Perform Get ", err)
			}
		if resp == nil {

			}
		time.Sleep(10 * time.Millisecond) // Delay slightly between requests
    }
}

