package main

import (
    "flag"
    "net/http"
    "encoding/json"
    "fmt"
    "log"
    "os"
    "runtime"
)

var addr = flag.String("addr", ":8080", "http service address")

var response = "Initial\n"

func handler(w http.ResponseWriter, req *http.Request) {

	log.Println(req.URL)
	if req.URL.Path != "/" {
			http.Error(w, "Not found", 404)
			return
	}
	if req.Method == "GET" {
			w.Header().Set("Content-Type", "text/plain")
			w.Write([]byte(response))
            fmt.Println(req.RemoteAddr)
//			http.Error(w, "Method not allowed", 405)
			return
	}
	if req.Method == "PUT" {
//			http.Error(w, "Method not allowed", 405)
//            defer req.Body.Close()
//			body, err := io.ReadAll(req.Body)
//			if err != nil {
//				fmt.Println("Failed to Read Body ", err)
//			}
 //           fmt.Println(body)
			return
	}
//	http.ServeFile(w, r, "/root/home.html")
}

func main() {
    startClients()
	http.HandleFunc("/", handler)
	log.Printf("About to listen on 8080. Go to https://127.0.0.1:8080/")
	// err := http.ListenAndServeTLS(":8080", "cert.pem", "key.pem", nil)
	err := http.ListenAndServe(*addr, nil)
	log.Fatal(err)
}

func startClients() {
		numClients := MaxParallelism()
		fmt.Println("Client: Number of Cores ", numClients)
        log.Println(numClients)
        numClients = 10
        // start at 1 to account for the server
        for i := 1; i < numClients; i++ {
                client := newClient()
                go client.run()
        }
}

func MaxParallelism() int {
        maxProcs := runtime.GOMAXPROCS(0)
        numCPU := runtime.NumCPU()
        if maxProcs < numCPU {
                return maxProcs
        }
        return numCPU
}

func stringtest() {
    s := make([]string, 1, 4)
    s[0] = "filename"
    sfinal := []string{"test", "test1"}
    dep_string := append(s[:1], sfinal...)

    fmt.Println("dep_String is ", dep_string)
    pagesJson, err := json.Marshal(dep_string)
    if err != nil {
        fmt.Println("Cannot encode to JSON ", err)
        log.Fatal("Cannot encode to JSON ", err)
    }
    fmt.Println(string(pagesJson))
    fmt.Fprintf(os.Stdout, "%s", pagesJson)
}