package main

import (
        "log"
        "os"
        "bytes"
        "time"
)

func main() {
        log.SetOutput(os.Stdout)
        log.Println("In main")
	hub := newHub()
	go hub.run()
        startClients(hub)
        var buffer bytes.Buffer
        buffer.WriteString("Begin")
        time.Sleep(100 * time.Millisecond)
        hub.broadcast <- buffer.Bytes()
        log.Println("Entering Loop")
        for {
         time.Sleep(100 * time.Millisecond)
        }
}
