// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"log"
	"time"
        "strconv"
        "bytes"
        "os"
        "runtime"

)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// Buffered channel of outbound messages.
	send chan []byte
}

// messagePump pumps messages to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) messagePump() {
	defer func() {
		c.hub.unregister <- c
	}()
        log.SetOutput(os.Stdout)
        log.Println("Entering Message Pump")
        var i = 0
        var buffer bytes.Buffer
	for {
                message, ok := <-c.send
                if !ok {
                                // The hub closed the channel.
                                return
                        }
                i += 1
                buffer.Write(message)
                buffer.WriteString(strconv.Itoa(i))
                log.Println(string(buffer.Bytes()))
		c.hub.broadcast <- buffer.Bytes()
	}
        log.Println("Exiting MessagePump")
}

// startClients handles starting the clients.
func startClients(hub *Hub) {
        numClients := MaxParallelism()
        log.Println(numClients)
	for i := 0; i < numClients; i++ {
		client := &Client{hub: hub, send: make(chan []byte, 256)}
		client.hub.register <- client
		// Allow collection of memory referenced by the caller by doing all work in
		// new goroutines.
		go client.messagePump()
	}
}

func MaxParallelism() int {
	maxProcs := runtime.GOMAXPROCS(0)
	numCPU := runtime.NumCPU()
	if maxProcs < numCPU {
		return maxProcs
	}
	return numCPU
}
