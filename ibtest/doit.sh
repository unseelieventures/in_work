#/bin/bash
sudo docker build --tag app_build -f Dockerfile.build .
sudo docker run --name app_build_1 -d app_build
sudo docker cp app_build_1:/usr/src/app/src/main/java/com/unseelieventures/app/avro/User.java .
sudo docker cp app_build_1:/root/.m2/repository/com/unseelieventures/app/java-archive/1.0-SNAPSHOT/java-archive-1.0-SNAPSHOT-jar-with-dependencies.jar target/java-archive-1.0-SNAPSHOT.jar
sudo docker stop app_build_1
sudo docker rm -v app_build_1
sudo docker rmi app_build
sudo docker build --tag app_run -f Dockerfile .
sudo docker run --name app_run_1 --add-host="ibhost:192.168.9.227" app_run
#sudo docker rm -v app_run_1
#sudo docker rmi app_run
#sudo docker rm -v $(sudo docker ps -a -q -f status=exited)
#sudo docker rmi $(sudo docker images -f "dangling=true" -q)
