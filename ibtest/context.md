## Context

IXIA has a Vision One packet broker appliance that contains a set of rules based on traffic patterns. In an environment that contains a Cisco APIC, changes to the APIC can invalidate the rules in the IXIA Vision One. 
IXIA needs a solution that will monitor for changes that are made in the APIC and change the rules in the IXIA Vision One. IXIA has an existing Packet Broker and Arganteal has an existing compiler but needs a new Change Data Capture (CDC) system.
 
![](embed:Context)

### IXIA Vision One

The IXIA Vision One System maintains a store of rules to filter and forward traffic. It has an existing REST API:

- Trade ID
- Date
- Current trade value in US dollars
- Counterparty ID

### Change Data Capture System

The Change Data Capture System detects changes in the Cisco APIC. This includes information about tenants, xxx, xxx
