/* Copyright (C) 2013 Interactive Brokers LLC. All rights reserved.  This code is subject to the terms
 * and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable. */

package com.unseelieventures.app;

import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Thread;

import com.ib.client.Contract;
import com.ib.contracts.StkContract;

import io.opentracing.Tracer;
import io.opentracing.Span;
import io.opentracing.Scope;
import io.opentracing.tag.Tags;
import io.opentracing.log.Fields;

public class SampleHistoryRequest extends SimpleWrapper {
   private enum Status { None, SecDef, SecDefFMF, Rfq, Ticks, Done, Error };

   private Object m_mutex = new Object();
   private Status m_status = Status.None;

   private Contract m_contract = null;
   
   private final Tracer tracer;


   public SampleHistoryRequest(Tracer tracer) {
   
   		this.tracer = tracer;

   }

   public void requestHistory() throws Exception {
      int clientId = 2;	
      String logStr = null;
      	
      Span requestHistoryspan = tracer.buildSpan("requestHistory").start();
      Scope scope = tracer.scopeManager().activate(requestHistoryspan, false);
      connect(clientId);

      if (client() != null && client().isConnected()) {
      String msg = "Server version=" + client().serverVersion();
      scope.span().log(ImmutableMap.of("event", "string-format", "value", msg));
      consoleMsg(msg);
      Span requestHistoricalDataspan = tracer.buildSpan("requestHistorical Data").start();
      Scope scope2 = tracer.scopeManager().activate(requestHistoricalDataspan, false);
      	 try {
            synchronized (m_mutex) {
               if (client().serverVersion() < 42) {
                  Tags.ERROR.set(requestHistoricalDataspan, true);
                  requestHistoricalDataspan.log(ImmutableMap.of(Fields.EVENT, "error", Fields.MESSAGE, String.format("Sample will not work with TWS older that 877")));
                  error ("Sample will not work with TWS older that 877");
               }

               while (m_status != Status.Done &&
                     m_status != Status.Error) {

                  if (m_status == Status.None) {
                     m_contract = new StkContract("IBM");
                     m_contract.currency("USD");
                     m_contract.exchange("Smart");
                     if (m_status != Status.Error &&
                         m_status != Status.SecDef) {
						 Date mCurrRequestDateTime = getLatestDownloadDate();
						 SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
						 String requestDateTimeStr = formatter.format(mCurrRequestDateTime);
						 logStr = String.format("Send Historical Data Request For contract=%s requestDateTimeStr=%s ", "IBM", requestDateTimeStr);
                		 scope2.span().log(ImmutableMap.of("event", "string-format", "value", logStr));
                         System.out.println(logStr);
                         client().reqHistoricalData(1, m_contract, "20181126 21:39:33", "10 D", "1 day", "TRADES", 1, 1, null);
                     }
                  }
                  scope2.span().log(ImmutableMap.of("event", "string-format", "value", String.format("Mutex Wait")));
                  m_mutex.wait();
                  scope2.span().log(ImmutableMap.of("event", "string-format", "value", String.format("Mutex Finish")));
               }
            }
         } catch (Exception e) {
         	Tags.ERROR.set(requestHistoricalDataspan, true);
    		requestHistoricalDataspan.log(ImmutableMap.of(Fields.EVENT, "error", Fields.ERROR_OBJECT, e, Fields.MESSAGE, e.getMessage()));
			e.printStackTrace();
		 }
         finally {
            disconnect();
            requestHistoricalDataspan.finish();
         } 
         if (m_status == Status.Done) {
         	scope.span().log(ImmutableMap.of("event", "string-format", "value", String.format("Done")));
            msg = "Done";
            consoleMsg(msg);
         }
      }
      else {
        Tags.ERROR.set(requestHistoryspan, true);
    	requestHistoryspan.log(ImmutableMap.of(Fields.EVENT, "error", Fields.MESSAGE, String.format("Client failed to connect")));
    	requestHistoryspan.finish();
      	error ("Client failed to connect");
      }
   }

	private Date getLatestDownloadDate() {
		//the day before at 12midnight
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.DATE, -1);
		Date date1DayBefore = cal.getTime();
		return date1DayBefore;
	}

   public void error(String str) {
      consoleMsg("Error=" + str);
      synchronized (m_mutex) {
         m_status = Status.Error;
         m_mutex.notify();
      }
   }

   public void error(int id, int errorCode, String errorMsg) {
      consoleMsg("Error id=" + id + " code=" + errorCode + " msg=" + errorMsg);
      if (errorCode >= 2100 && errorCode < 2200) {
         return;
      }
      synchronized (m_mutex) {
         m_status = Status.Error;
         m_mutex.notify();
      }
   }
}
