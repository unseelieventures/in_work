
package com.unseelieventures.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.lang.String;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.*;
import io.opentracing.Tracer;
import io.opentracing.util.GlobalTracer;
import io.jaegertracing.Configuration;
import io.jaegertracing.internal.JaegerTracer;
import io.jaegertracing.Configuration.ReporterConfiguration;
import io.jaegertracing.Configuration.SamplerConfiguration;
import io.jaegertracing.Configuration.SenderConfiguration;
import io.jaegertracing.internal.samplers.ConstSampler;


public class Main {
   /* ***************************************************************
    * Main Method
    *****************************************************************/
   public static void main(String[] args) {

	try  {
		Properties config = loadConfig(args);
		configureGlobalTracer(config, "stock-test");
		SampleHistoryRequest hrq = new SampleHistoryRequest(GlobalTracer.get());
		hrq.requestHistory();
	} catch (Exception e) {
		e.printStackTrace();
	}
	 Schema schema = null;
     try {
        schema = new Schema.Parser().parse(new File("/root/user.avsc"));
      } catch (Exception e) {
         e.printStackTrace();
      }
        GenericRecord user1 = new GenericData.Record(schema);
	user1.put("name", "Alyssa");
	user1.put("favorite_number", 256);
	// Leave favorite color null
	GenericRecord user2 = new GenericData.Record(schema);
	user2.put("name", "Ben");
	user2.put("favorite_number", 7);
	user2.put("favorite_color", "red");
	// Leave favorite color null
      
      	// Serialize user1 and user2 to disk
	File file = null;
      try {
        file = new File("/root/users.avro");
	DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
	DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
	dataFileWriter.create(schema, file);
	dataFileWriter.append(user1);
	dataFileWriter.append(user2);
	dataFileWriter.close();
      } catch (Exception e) {
         e.printStackTrace();
      }
      
      try {
      // Deserialize users from disk
	DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
	DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, datumReader);
	GenericRecord user = null;
	while (dataFileReader.hasNext()) {
		// Reuse user object by passing it to next(). This saves us from
		// allocating and garbage collecting many objects for files with
		// many items.
		user = dataFileReader.next(user);
		System.out.println(user);
	 }
       } catch (Exception e) {
         e.printStackTrace();
      }      
   }
   static Properties loadConfig(String [] args)
        throws IOException
    {
        String file = "/root/tracer_config.properties";
        if (args.length > 0)
            file = args[0];

        FileInputStream fs = new FileInputStream(file);
        Properties config = new Properties();
        config.load(fs);
        return config;
    }
    static void configureGlobalTracer(Properties config, String componentName) {
   	   Tracer tracer = null;
	   SamplerConfiguration samplerConfig = new SamplerConfiguration()
					.withType(ConstSampler.TYPE)
					.withParam(1);
				SenderConfiguration senderConfig = new SenderConfiguration()
					.withAgentHost(config.getProperty("jaeger.reporter_host"))
					.withAgentPort(Integer.decode(config.getProperty("jaeger.reporter_port")));
				ReporterConfiguration reporterConfig = new ReporterConfiguration()
					.withLogSpans(true)
					.withFlushInterval(1000)
					.withMaxQueueSize(10000)
					.withSender(senderConfig);
				tracer = new Configuration(componentName).withSampler(samplerConfig).withReporter(reporterConfig).getTracer();
   	   GlobalTracer.register(tracer);
   }
}
